class User < ActiveRecord::Base
  attr_accessible :username, :password, :ensure_session_token

  validates :username, :password, presence: true
  validates :username, uniqueness: true
  validates :password, length: {minimum: 6}

  before_validation :ensure_session_token

  def self.find_by_credentials(username, secret)
    user = User.find_by_username(username)
    return user if user && user.is_password?(secret)

    nil
  end

  def is_password?(secret)
    self.password == secret
  end

  def reset_session_token
    self.session_token = self.class.generate_session_token
  end

  def self.generate_session_token
    SecureRandom::urlsafe_base64(16)
  end

  def ensure_session_token
    self.session_token ||= self.class.generate_session_token
  end
end
