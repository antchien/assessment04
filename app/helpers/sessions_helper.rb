module SessionsHelper

  def current_user
    user = User.find_by_session_token(session[:session_token])
  end

  def logged_in?
    !!current_user
  end

  def login_user!(user)
    user.reset_session_token
    if user.save
      session[:session_token] = user.session_token
    else
      flash[:errors] ||= []
      flash[:errors] = user.errors.full_messages
    end
  end

  def logout_user!
    user = current_user
    session[:session_token] = nil
    user.reset_session_token
  end

end
