class SessionsController < ApplicationController

  def new
  end

  def create
    user = User.find_by_credentials(params[:user][:username], params[:user][:passowrd])
    if user
      login_user!(user)

      redirect_to links_url
    else
      flash[:errors] = user.errors.full_messages

      redirect_to new_session_url
    end
  end


end
