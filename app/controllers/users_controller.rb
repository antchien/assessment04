class UsersController < ApplicationController

  def index
    @users = User.all
  end

  def new
  end

  def create
    user = User.new(params[:user])
    if user.save
      login_user!(user)

      redirect_to links_url
    else
      flash[:errors] ||= []
      flash[:errors] = user.errors.full_messages

      redirect_to new_user_url
    end

  end

  def show
    @user = User.find(params[:id])
  end

  def edit
  end

  def update
    current_user.update_attributes(params[:user])

    redirect_to current_user
  end

  def destroy
  end


end
